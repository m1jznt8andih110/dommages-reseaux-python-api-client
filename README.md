# Requirements

* Python >= 3.6
* [Requests](https://requests.readthedocs.io/en/master/): `pip install requests`

# Usage

```python
import DommagesReseauxApiClient

api_client = DommagesReseauxApiClient()

api_client.login()

result = api_client.get_equipment_by_coordinates("RA_POTEAU", 46.7537, 2.2124)
```

# Equipment types

## Aerial network (`RA_*`)

* `RA_POTEAU`: Street pole
* `RA_CABLE`: Cable

## Underground network (`RS_*`)

* `RS_CABLE`: Cable
* `RS_TRAPPE`: Manhole cover

## Street furniture

* `MOB_ARMOIRE`: Street cabinet
* `MOB_COFFRET`: Box on street pole / wall
* `CAB_MOBILIER`: Telephone box

# Links

* [dommages-reseaux.orange.fr](https://dommages-reseaux.orange.fr/dist-dommages/app/home)
* [Dommages Réseaux - 1101011.xyz](https://1101011.xyz/com.orange.labs.reseau1013)