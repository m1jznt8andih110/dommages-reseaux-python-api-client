import requests

BASE_URL = 'https://dommages-reseaux.orange.fr'

class DommagesReseauxApiClient(object):
    
    def __init__(self, token = None):
        self.token = token
        self.build_cookie_jar()

        self.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36',
            'Referer': 'https://dommages-reseaux.orange.fr/dist-dommages/app/signaler',
            'Origin': 'https://dommages-reseaux.orange.fr',
            'Accept': 'application/json'
        }

    def build_cookie_jar(self):
        self.cookies = dict()

        if self.token is not None:
            self.cookies['token'] = self.token

    def login(self, username = "apii0000", password = "apii0000"):
        data = {
            'username': username,
            'password': password
        }

        r = requests.post(BASE_URL + '/api/auth/login', json = data, headers = self.headers)

        if r.status_code != requests.codes.ok:
            raise Exception("Incorrect response code")

        if 'token' not in r.json():
            raise Exception("Login failed (no token provided in response)")

        self.token = r.json()['token']
        self.build_cookie_jar()

        return True

    def get(self, uri, query_params = None):
        r = requests.get(BASE_URL + uri, params = query_params, headers = self.headers, cookies = self.cookies)

        return r.json()

    def post(self, uri, data = None):
        r = requests.post(BASE_URL + uri, json = data, headers = self.headers, cookies = self.cookies)

        return r.json()

    def get_select_list(self):
        return self.get('/api/v1/select-list')

    def get_derangement_type(self):
        return self.get('/api/v1/derangement-type')

    def get_addresses_by_coordinates(self, latitude, longitude):
        params = {
            'mode': 'geoCoordinates',
            'geoCodeY': latitude,
            'geoCodeX': longitude,
            'typography': 'RICH CASE LETTER'
        }

        return self.get('/api/v1/oras/addresses', params)

    def get_addresses_by_query(self, query):
        params = {
            'mode': 'fullText',
            'fullText': query,
            'typography': 'RICH CASE LETTER'
        }

        return self.get('/api/v1/oras/addresses', params)

    def get_equipment_by_coordinates(self, type, latitude, longitude):
        return self.get(f"/api/v1/equipement/{type}/around/latitude/{latitude}/longitude/{longitude}")

    def get_token(self):
        return self.token
    